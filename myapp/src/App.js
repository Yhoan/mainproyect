import React from 'react';
import {BrowserRouter as Router,Route,Link } from 'react-router-dom';

import list from "./list/list.json";
import Listas from './component/Listas';
import NameForm from './component/nameForm';
import Post from './component/Post'
import './App.css';

import img from './img/amorcito.jpg';

class Imagen extends React.Component{

  state={
    show:false
  }
  //cambio del estado(muestreo de la imagen)
  hangle=()=>{
    this.setState({show: !this.state.show})
  }

  //funcion para añadir estilos al documento 
  button(){
    return {
      position:"absolute",
      right:"0",
      top:"0",

      color:"blue",
      fontSize:"20px",
      borderRadius:"20%"
    }
  }

  render(){
    if(this.state.show){
      return <div>
        <button onClick={this.hangle} style={this.button()}>esconder</button>
        <img src={img} alt="imagen no existente" className="img" />
      </div>
    }else{
      return <div>
        <button onClick={this.hangle} style={this.button()} >Y mi novia hermosa es:</button>
      </div>
    }
  }
}

class App extends React.Component{

  state={
    list:list
  }

  addTask=(name,edad,parent)=>{
    const newTask={
      id:this.state.list.length ,
      name:name,
      edad:edad,
      parent:parent
    }
    this.setState({
      list: [...this.state.list, newTask]
    })
  }

  deleteTask = (id) =>{
    this.setState({
      list: this.state.list.filter(list => list.id !== id)
    })
  }

  render(){
    return <div className="app">

      <h1 style={title} >Mi nombre es Yhoan Mateo</h1>

      <section id="section" >
        <Router>
          <Link to="/" >Pagina principal</Link><br/>
          <Link to="/image" >Imagen</Link><br/>
          <Link to="/post" >Posts </Link>
          <Route exact path="/" render={()=>{
            return <div>
              <h3>Mi familia es:</h3>

              <Listas listas={this.state.list} deleteTask={this.deleteTask} />
              <NameForm addTask={this.addTask} />
            </div>
          } } />
          <Route path="/image" component={Imagen} />
          <Route path="/post" component={Post} />
        </Router>
      </section>
      
    </div>
  }
}

const title ={
  background:"green",
  textAlign:"center"
}


export default App;