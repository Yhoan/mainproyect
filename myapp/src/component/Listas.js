import React from 'react';
import PropsTypes from 'prop-types';

class Listas extends React.Component{

    render(){
        return this.props.listas.map(e=> <h3 key={e.id} >
            Nombre: {e.name} <br/> Edad: {e.edad} <br/> Parentesco: {e.parent} <br/>
            <button 
                style={btDelete} 
                //onClick, desde la propiedad deleteTask va a enviar el valor del id
                onClick={this.props.deleteTask.bind(this, e.id )} 
            >Eliminar</button>
            <br/> <br/>
        </h3> )
    }
}

const btDelete={
    borderRadius:"20%",
    background:"red"
}

//Proptypes hace necesario el translado de ciertos tipos de datos o propiedades
Listas.PropsTypes={
    listas: PropsTypes.array.isRequired
}

export default Listas;