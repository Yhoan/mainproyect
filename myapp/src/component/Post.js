import React, { Component } from 'react'

export default class Post extends Component {

    state={
        post:[]
    }

    //Funcion promesa, en dode la pagina espera a que se cargen los datos para devolverlos y 
    //continuar con el procedimiento async/await
    async componentDidMount(){
        //fetch: se utiliza para hacer una peticion a un servidor backend
        const res = await fetch('https://jsonplaceholder.typicode.com/posts');
        const data = await res.json();
        this.setState({post:data})
    }

    render() {
        return (
            <div>
                <h1 style={{textAlign:"center"}} >
                    Posts
                </h1>
                <h3>
                    {this.state.post.map(post=> {return <div key={post.id} >
                        <h1>{post.title}</h1>
                        <p>{post.body}</p>
                    </div>} )}
                </h3>
            </div>
        )
    }
}
