import React from 'react';

class nameForm extends React.Component{

    onSubmit= e =>{
        this.props.addTask(this.state.name,this.state.edad,this.state.parent);
        e.preventDefault();
    }

    onChange= e =>{
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    state={
        name:'',
        edad:'',
        parent:''
    }

    render(){
        return <form onSubmit={this.onSubmit} >
            <textarea 
                name="name"
                placeholder="escribe un nombre" 
                onChange={this.onChange} 
                value={this.state.name}
            /> <br/>

            <textarea 
                name="edad"
                placeholder="escribe su edad" 
                onChange={this.onChange} 
                value={this.state.edad}
            /> <br/>

            <textarea 
                name="parent"
                placeholder="escribe su parentesco" 
                onChange={this.onChange} 
                value={this.state.parent}
            />

            <input type="submit" />
        </form>
    }
}

export default nameForm;